const { test, expect } = require("@playwright/test");
let id = "653e1a56c1184dc65851738b";
test("Check update successfully incase edit valid data into all fields", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 50,
      budgetOptimizeOn: true,
      name: "campaign 7 update",
      objectiveType: "TRAFFIC",
    },
  });
  expect(updateResponse.ok()).toBeTruthy();
  const updateCampaign = await updateResponse.json();
  console.log({ updateCampaign });
  const getByIdResponse = await request.get(`/api/eac/campaigns/${id}`);
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_TOTAL",
    budget: 50,
    budgetOptimizeOn: true,
    name: "campaign 7 update",
    objectiveType: "TRAFFIC",
  });
});

test("Check update successfully incase edit valid data into any fields", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {
      name: "update name",
      budgetMode: "BUDGET_MODE_TOTAL",
    },
  });
  // expect(updateResponse.ok()).toBeTruthy();
  const updateCampaign = await updateResponse.json();
  console.log("abc", { updateCampaign });
  const getByIdResponse = await request.get(`/api/eac/campaigns/${id}`);
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    name: "update name",
    budgetMode: "BUDGET_MODE_TOTAL",
  });
});

test("Check update unsuccessfully incase clear data into all fields", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {},
  });
  expect(updateResponse.status()).toBe(200);
  const updateCampaign = await updateResponse.json();
  console.log({ updateCampaign });
  expect(updateCampaign.statusCode).toBe(400);
  expect(updateCampaign.messageCode).toContain(
    "error.campaign.not_allow_update_budget_mode"
  );
});

test("Check update successfully and return the previous value incase change objectiveType value", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {
      objectiveType: "VIDEO_VIEWS",
    },
  });
  expect(updateResponse.ok()).toBeTruthy();
  const updateCampaign = await updateResponse.json();
  console.log({ updateCampaign });
  const getByIdResponse = await request.get(`/api/eac/campaigns/${id}`);
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    objectiveType: "TRAFFIC",
  });
});

test("Check update successfully incase change budgetMode of campaign has budgetMode 'BUDGET_MODE_INFINITE'", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
    },
  });
  expect(updateResponse.ok()).toBeTruthy();
  const updateCampaign = await updateResponse.json();
  console.log({ updateCampaign });
  const getByIdResponse = await request.get(`/api/eac/campaigns/${id}`);
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_TOTAL",
  });
});

test("Check update unsuccessfully incase change budgetMode of campaign hasn't budgetMode 'BUDGET_MODE_INFINITE'", async ({
  request,
}) => {
  const updateResponse = await request.patch(`/api/eac/campaigns/${id}`, {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
    },
  });
  expect(updateResponse.status()).toBe(400);
  const updateCampaign = await updateResponse.json();
  console.log({ updateCampaign });
  expect(updateCampaign.statusCode).toBe(400);
  expect(updateCampaign.messageCode).toContain(
    "error.campaign.not_allow_update_budget_mode"
  );
});
