// @ts-check
const { test, expect } = require("@playwright/test");

test("Check get list campaigns successfully and correctly", async ({
  request,
}) => {
  const response = await request.get("/api/eac/campaigns");
  expect(response.ok()).toBeTruthy();
  const campaigns = await response.json();
  // console.log({ campaigns });
  expect(campaigns).toHaveProperty("requestId");
  expect(campaigns).toHaveProperty("result");
  expect(campaigns.result).toHaveProperty("paging");
  expect(campaigns.result).toHaveProperty("content");
  expect(campaigns.result.paging).toHaveProperty("limit");
  expect(campaigns.result.paging).toHaveProperty("page");
  expect(campaigns.result.paging).toHaveProperty("total");
  for (let i = 0; i < campaigns.result.content; i++) {
    expect(campaigns.result.content[i]).toHaveProperty("budgetMode");
    expect(campaigns.result.content[i]).toHaveProperty("budgetOptimizeOn");
    expect(campaigns.result.content[i]).toHaveProperty("createdAt");
    expect(campaigns.result.content[i]).toHaveProperty("isDraft");
    expect(campaigns.result.content[i]).toHaveProperty("localUserId");
    expect(campaigns.result.content[i]).toHaveProperty("name");
    expect(campaigns.result.content[i]).toHaveProperty("objectiveType");
    expect(campaigns.result.content[i]).toHaveProperty("updatedAt");
    expect(campaigns.result.content[i]).toHaveProperty("userId");
    expect(campaigns.result.content[i]).toHaveProperty("_id");
  }
});

test("Check get list campaign successfully and correct incase filter by page and limit ", async ({
  request,
}) => {
  const response = await request.get(`/api/eac/campaigns?page=1&limit=10`, {});
  expect(response.ok()).toBeTruthy();
  const campaigns = await response.json();
  // console.log({ campaigns });
  expect(campaigns.result).toEqual(
    expect.objectContaining({ content: expect.any(Object) })
  );
  expect(campaigns.result.paging.page).toBe(1);
  expect(campaigns.result.paging.limit).toBe(10);

  if (campaigns.result.paging.total < campaigns.result.paging.limit) {
    expect(campaigns.result.content.length).toBe(campaigns.result.paging.total);
  } else {
    expect(campaigns.result.content.length).toBe(campaigns.result.paging.limit);
  }
});
