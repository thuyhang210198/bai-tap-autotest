// @ts-check
const { test, expect } = require("@playwright/test");

test("Check create new campaign successfully incase input valid data into all fields", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "campaign 2",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    budget: 100000,
    budgetOptimizeOn: false,
    name: "campaign 2",
    objectiveType: "TRAFFIC",
  });
  // expect(createCampaign.result).toMatchObject(getById.result);
});

test("Check create campaign successfully incase input valid data into the required fields and no input data into the optional fields", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      name: "campaign 3",
      objectiveType: "WEB_CONVERSIONS",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    name: "campaign 3",
    objectiveType: "WEB_CONVERSIONS",
  });
});

test("Check create new campaign unsuccessfully incase input valid data into the optional fields and no input data into the required fields", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budget: 60,
      budgetOptimizeOn: true,
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "budgetMode must be one of the following values: BUDGET_MODE_DAY, BUDGET_MODE_TOTAL, BUDGET_MODE_INFINITE"
  );
  expect(createCampaign.errors).toMatchObject({
    budgetMode:
      "Budget Mode must be one of the following values: BUDGET_MODE_DAY, BUDGET_MODE_TOTAL, BUDGET_MODE_INFINITE",
    name: "Name must be shorter than or equal to 512 characters",
    objectiveType:
      "Objective Type must be one of the following values: TRAFFIC, WEB_CONVERSIONS, PRODUCT_SALES, VIDEO_VIEWS",
  });
});

test("Check create new campaign unsuccessfully incase no input data all fields", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {},
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "budgetMode must be one of the following values: BUDGET_MODE_DAY, BUDGET_MODE_TOTAL, BUDGET_MODE_INFINITE"
  );
  expect(createCampaign.errors).toMatchObject({
    budgetMode:
      "Budget Mode must be one of the following values: BUDGET_MODE_DAY, BUDGET_MODE_TOTAL, BUDGET_MODE_INFINITE",
    name: "Name must be shorter than or equal to 512 characters",
    objectiveType:
      "Objective Type must be one of the following values: TRAFFIC, WEB_CONVERSIONS, PRODUCT_SALES, VIDEO_VIEWS",
  });
});

//objectiveType
test("Check create campaign unsuccessfully incase no enter data into the objectiveType field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "campaign 2",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "objectiveType must be one of the following values: TRAFFIC, WEB_CONVERSIONS, PRODUCT_SALES, VIDEO_VIEWS"
  );
});

test("Check create campaign unsuccessfully incase enter invalid data into the objectiveType field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "campaign 2",
      objectiveType: "test",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "objectiveType must be one of the following values: TRAFFIC, WEB_CONVERSIONS, PRODUCT_SALES, VIDEO_VIEWS"
  );
});

test("Check create campaign successfully incase enter valid data into the objectiveType field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "campaign 4",
      objectiveType: "VIDEO_VIEWS",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    budget: 100000,
    budgetOptimizeOn: false,
    name: "campaign 4",
    objectiveType: "VIDEO_VIEWS",
  });
  // expect(createCampaign.result).toMatchObject(getById.result);
});

//budgetOptimizeOn
test("Check create campaign successfully and return 'false' value incase no enter data into the budgetOptimizeOn field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      name: "campaign 4",
      objectiveType: "VIDEO_VIEWS",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetOptimizeOn: false,
  });
});

test("Check create campaign unsuccessfully incase enter invalid data into the budgetOptimizeOn field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: "test",
      name: "campaign 4",
      objectiveType: "VIDEO_VIEWS",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "budgetOptimizeOn must be a boolean value"
  );
});

test("Check create campaign successfully value incase enter valid value into the budgetOptimizeOn field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "campaign 4",
      objectiveType: "VIDEO_VIEWS",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetOptimizeOn: false,
  });
});

//name
test("Check create campaign unsuccessfully incase no input data into the name field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "name must be shorter than or equal to 512 characters"
  );
});

test("Check create campaign successfully incase input data contains text, number and special characters into the name field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "Campaign 6 #",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    budget: 100000,
    budgetOptimizeOn: false,
    name: "Campaign 6 #",
    objectiveType: "TRAFFIC",
  });
});

test("Check create campaign successfully incase input data less than 512 characters into the name field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "Campaign 6",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    budget: 100000,
    budgetOptimizeOn: false,
    name: "Campaign 6",
    objectiveType: "TRAFFIC",
  });
});

test("Check create campaign successfully incase input data equal 512 characters into the name field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "4mWKyPGUJyRFVTvYxC4SPFw3fLRG18LPZyiesLMOhzh70sIw9Eou5dCcz3Z9CUScr4GtjFdslS4ygNZRPMbv1EXlk149fqjJlTHAnQnV2sWElDgYNfcoDiWTnexMzZLsPVnLCk3ZAQUNiz5E5uWg4XBM82C6Sib46h1DMxWf2n0KEFvROhCHUvijMeaQe249jJyb8aeMrcMSqEZgTeD4s3eyeWs7i1gMQ0zXAWmsEFvJnc1535NSqmYLjudo7wdO2Pg8u1H2Sy5NmMzxMgSJxAyTVpftmCjnRqWHIP79mT4Ofc1fEFd71dXm910H1M9TlUrBmErC8Bj7xUbVTXLV8069y1tK6tBzZdAoRhDQRqhEMa7xUnTY2r0oIyqDrpCDOnazrEsVfcAyrYnVeNLwRk412RYsmPJ2a4zwgcVjMrIDBwjOmPNLUS7HI95ANNZmYcZWuK7gK3LU4ISOUYsjqfxXHRNo4iDOXCdiHqAQFoXtajYi6JoGXp8IE75BJ00f",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budgetMode: "BUDGET_MODE_INFINITE",
    budget: 100000,
    budgetOptimizeOn: false,
    name: "4mWKyPGUJyRFVTvYxC4SPFw3fLRG18LPZyiesLMOhzh70sIw9Eou5dCcz3Z9CUScr4GtjFdslS4ygNZRPMbv1EXlk149fqjJlTHAnQnV2sWElDgYNfcoDiWTnexMzZLsPVnLCk3ZAQUNiz5E5uWg4XBM82C6Sib46h1DMxWf2n0KEFvROhCHUvijMeaQe249jJyb8aeMrcMSqEZgTeD4s3eyeWs7i1gMQ0zXAWmsEFvJnc1535NSqmYLjudo7wdO2Pg8u1H2Sy5NmMzxMgSJxAyTVpftmCjnRqWHIP79mT4Ofc1fEFd71dXm910H1M9TlUrBmErC8Bj7xUbVTXLV8069y1tK6tBzZdAoRhDQRqhEMa7xUnTY2r0oIyqDrpCDOnazrEsVfcAyrYnVeNLwRk412RYsmPJ2a4zwgcVjMrIDBwjOmPNLUS7HI95ANNZmYcZWuK7gK3LU4ISOUYsjqfxXHRNo4iDOXCdiHqAQFoXtajYi6JoGXp8IE75BJ00f",
    objectiveType: "TRAFFIC",
  });
});

test("Check create campaign unsuccessfully incase input data more than 512 characters into the name field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budget: 100000,
      budgetOptimizeOn: false,
      name: "24mWKyPGUJyRFVTvYxC4SPFw3fLRG18LPZyiesLMOhzh70sIw9Eou5dCcz3Z9CUScr4GtjFdslS4ygNZRPMbv1EXlk149fqjJlTHAnQnV2sWElDgYNfcoDiWTnexMzZLsPVnLCk3ZAQUNiz5E5uWg4XBM82C6Sib46h1DMxWf2n0KEFvROhCHUvijMeaQe249jJyb8aeMrcMSqEZgTeD4s3eyeWs7i1gMQ0zXAWmsEFvJnc1535NSqmYLjudo7wdO2Pg8u1H2Sy5NmMzxMgSJxAyTVpftmCjnRqWHIP79mT4Ofc1fEFd71dXm910H1M9TlUrBmErC8Bj7xUbVTXLV8069y1tK6tBzZdAoRhDQRqhEMa7xUnTY2r0oIyqDrpCDOnazrEsVfcAyrYnVeNLwRk412RYsmPJ2a4zwgcVjMrIDBwjOmPNLUS7HI95ANNZmYcZWuK7gK3LU4ISOUYsjqfxXHRNo4iDOXCdiHqAQFoXtajYi6JoGXp8IE75BJ00f",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "name must be shorter than or equal to 512 characters"
  );
});

//budget
test("Check create campaign successfully incase no input data into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_INFINITE",
      budgetOptimizeOn: false,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).not.toHaveProperty("budget");
});

test("Check create campaign successfully incase input number into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 70,
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budget: 70,
  });
});

test("Check create campaign unsuccessfully incase input text characters into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: "test",
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "budget must be a number conforming to the specified constraints"
  );
});

test("Check create campaign unsuccessfully incase input special characters into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: "@#$",
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain(
    "budget must be a number conforming to the specified constraints"
  );
});

test("Check create campaign unsuccessfully incase input less than 50 into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 10,
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain("error.mongo.validation_error");
});

test("Check create campaign successfully incase input equal 50 into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 50,
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budget: 50,
  });
});

test("Check create campaign successfully incase input equal 10000000 into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 10000000,
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.ok()).toBeTruthy();
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  const getByIdResponse = await request.get(
    `/api/eac/campaigns/${createCampaign.result._id}`
  );
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  console.log({ getById });
  expect(getById.result).toMatchObject({
    budget: 10000000,
  });
});

test("Check create campaign unsuccessfully incase input more than 10000000 into the budget field", async ({
  request,
}) => {
  const createResponse = await request.post("/api/eac/campaigns", {
    data: {
      budgetMode: "BUDGET_MODE_TOTAL",
      budget: 11000000,
      budgetOptimizeOn: true,
      name: "campaign 7",
      objectiveType: "TRAFFIC",
    },
  });
  expect(createResponse.status()).toBe(400);
  const createCampaign = await createResponse.json();
  console.log({ createCampaign });
  expect(createCampaign.statusCode).toBe(400);
  expect(createCampaign.messageCode).toContain("error.mongo.validation_error");
});
