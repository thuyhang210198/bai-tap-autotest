// @ts-check
const { test, expect } = require("@playwright/test");
let id = "653e1a56c1184dc65851738b";
test("Check get campaign by id successfully incase input valid id", async ({
  request,
}) => {
  const getByIdResponse = await request.get(`/api/eac/campaigns/${id}`);
  expect(getByIdResponse.ok()).toBeTruthy();
  const getById = await getByIdResponse.json();
  // console.log({ getById });
  expect(getById).toHaveProperty("requestId");
  expect(getById).toHaveProperty("result");
  expect(getById.result).toHaveProperty("_id");
  expect(getById.result).toHaveProperty("userId");
  expect(getById.result).toHaveProperty("localUserId");
  expect(getById.result).toHaveProperty("isDraft");
  expect(getById.result).toHaveProperty("budgetMode");
  expect(getById.result).toHaveProperty("budget");
  expect(getById.result).toHaveProperty("budgetOptimizeOn");
  expect(getById.result).toHaveProperty("name");
  expect(getById.result).toHaveProperty("objectiveType");
  expect(getById.result).toHaveProperty("createdAt");
  expect(getById.result).toHaveProperty("updatedAt"); // test
});
